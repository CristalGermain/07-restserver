require('./config/config');
const express = require('express');
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
const path = require('path');
const app = express();


// URLEncoded para leer los datos del request
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());


// CONFIGURACIÓN DE RUTAS
app.use(require('./routes/index.js'));

app.use(express.static(path.resolve(__dirname, '../public')));


// CONEXIÓN A BASE DE DATOS (EN MONGO)
mongoose.connect(process.env.URLDB, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true, useFindAndModify: false },
    (err, resp) => {
        if (err) throw err;
        console.log('Base de datos ONLINE');
    });


app.listen(process.env.PORT, () => {
    console.log('Escuchando puerto: ', process.env.PORT);
});