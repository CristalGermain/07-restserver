const jwt = require('jsonwebtoken');


//  VERIFICAR TOKEN

let verificaToken = (request, response, next) => {

    // De esta manera se obtienen los headers
    let token = request.get('Authorization');

    jwt.verify(token, process.env.SEED, (err, decoded) => {


        if (err) {
            return response.status(401).json({
                ok: false,
                err: {
                    message: "Token no válido",
                }
            });
        }

        request.usuario = decoded.usuario;
        next();

    });


};




//  VERIFICAR ADMIN

let verificaAdmin = (request, response, next) => {


    if (request.usuario.role === 'ADMIN_ROLE') {
        next();
    } else {
        response.json({
            ok: false,
            error: {
                message: 'Debes ser administrador para realizar esta acción'
            }
        });
    }



};

// VERIFICAR TOKEN PARA IMAGEN

let verificaTokenImg = (request, response, next) => {

    let token = request.query.token;

    jwt.verify(token, process.env.SEED, (err, decoded) => {


        if (err) {
            return response.status(401).json({
                ok: false,
                err: {
                    message: "Token no válido",
                }
            });
        }

        request.usuario = decoded.usuario;
        next();

    });
};




module.exports = {
    verificaToken,
    verificaAdmin,
    verificaTokenImg
};