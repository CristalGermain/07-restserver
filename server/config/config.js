// ---------------------------
//          PUERTO
// ---------------------------

process.env.PORT = process.env.PORT || 3000;



// ---------------------------
//          ENTORNO
// ---------------------------

process.env.NODE_ENV = process.env.NODE_ENV || 'dev';




// ---------------------------
//    VENCIMIENTO DEL TOKEN
// ---------------------------
//  60 segundos
//  60 minutos
//  24 horas
//  30 días
process.env.CADUCIDAD_TOKEN = '48h';




// ---------------------------
//    SEED DE AUTIENTICACIÓN
// ---------------------------

process.env.SEED = process.env.SEED || 'seed-de-desarrollo'





// ---------------------------
//        BASE DE DATOS
// ---------------------------
let urlDB;

if (process.env.NODE_ENV === 'dev') {
    urlDB = 'mongodb://127.0.0.1:27017/cafe'
} else {
    urlDB = process.env.MONGO_URI;
}

process.env.URLDB = urlDB;




// ---------------------------
//      GOOGLE CLIENT ID
// ---------------------------
process.env.CLIENT_ID = process.env.CLIENT_ID || '840745560959-gt1mlj4hvbc83uhmojkv01ucp9etiblh.apps.googleusercontent.com';