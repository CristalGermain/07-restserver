const express = require('express');
const bcrypt = require('bcrypt');
const _ = require('underscore');
const Usuario = require('../models/usuario');
const { verificaToken, verificaAdmin } = require('../middlewares/autenticacion');
const app = express();



app.get('/usuario', verificaToken, (request, response) => {

    //en query recaen los parámetros de la URL
    let inicio = request.query.inicio || 0;
    inicio = Number(inicio);
    let limite = Number(request.query.limite) || 5;

    Usuario.find({ estado: true }, 'nombre email role estado google img')
        .skip(inicio)
        .limit(limite)
        .exec((err, usuarios) => {
            if (err) {
                return response.status(400).json({
                    ok: false,
                    err
                })
            }

            Usuario.count({ estado: true }, (err, conteo) => {
                response.json({
                    ok: true,
                    conteo,
                    usuarios
                });
            });

            /* response.json({
                 ok: true,
                 usuarios
             });*/
        });


});

app.post('/usuario', [verificaToken, verificaAdmin], (request, response) => {
    let body = request.body;

    let usuario = new Usuario({
        nombre: body.nombre,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        role: body.role
    });


    usuario.save((err, usuarioDB) => {
            if (err) {
                return response.status(400).json({
                    ok: false,
                    err
                });
            }

            // usuarioDB.password = null;

            response.json({
                ok: true,
                usuario: usuarioDB
            });


        }) //palabra reservada de mongoose

});

app.put('/usuario/:id', [verificaToken, verificaAdmin], (request, response) => {
    let id = request.params.id;
    let body = _.pick(request.body, [
        'nombre', 'img', 'role'
    ]);
    // body.password = bcrypt.hashSync(body.password, 10);

    Usuario.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, usuarioBD) => {
        if (err) {
            return response.status(400).json({
                ok: false,
                err
            });
        }
        response.json({
            ok: true,
            usuario: usuarioBD
        });

    });


});


// Borrar físicamente de la BD
app.delete('/usuario/:id', [verificaToken, verificaAdmin], (request, response) => {

    let id = request.params.id;


    Usuario.findByIdAndRemove(id, (err, usuarioBorrado) => {
        if (err) {
            return response.status(400).json({
                ok: false,
                err
            });
        }
        if (usuarioBorrado === null) {
            return response.status(400).json({
                ok: false,
                err: {
                    message: 'Usuario no encontrado'
                }
            });
        }

        response.json({
            ok: true,
            usuario: usuarioBorrado
        });
    });

});

// Marcar una eliminación (sin borrar físicamente)
app.delete('/usuarioDelete/:id', [verificaToken, verificaAdmin], (request, response) => {

    let id = request.params.id;
    let body = { estado: false };


    Usuario.findByIdAndUpdate(id, body, { new: true }, (err, usuarioBorrado) => {
        if (err) {
            return response.status(400).json({
                ok: false,
                err
            });
        }
        if (!usuarioBorrado.estado) {
            return response.status(400).json({
                ok: false,
                err: {
                    message: 'Usuario no encontrado'
                }
            });
        }
        response.json({
            ok: true,
            usuario: usuarioBorrado
        });
    });

});

module.exports = app;