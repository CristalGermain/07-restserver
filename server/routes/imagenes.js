const express = require('express');
const fs = require('fs');
const path = require('path');

const { verificaTokenImg } = require('../middlewares/autenticacion');

const app = express();

app.get('/mostrarImagen/:tipo/:img', verificaTokenImg, (request, response) => {

    let tipo = request.params.tipo;
    let img = request.params.img;

    let pathImg = path.resolve(__dirname, `../../uploads/${tipo}/${img}`);

    if (fs.existsSync(pathImg)) {
        response.sendFile(pathImg);
    } else {
        let noImagePath = path.resolve(__dirname, '../assets/no-image.jpg');
        response.sendFile(noImagePath);
    }

});


module.exports = app;