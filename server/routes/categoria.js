const express = require('express');

const { verificaToken, verificaAdmin } = require('../middlewares/autenticacion');

const _ = require('underscore');

const app = express();

const Categoria = require('../models/categoria');


// MOSTRAR TODAS LAS CATEGORÍAS
app.get('/categoria', verificaToken, (request, response) => {

    Categoria.find({})
        .sort('descripcion') // Ordena por descripción
        .populate('usuario', 'nombre email') //trae los datos de otras colecciones donde encuentre coincidencia de Id
        .exec((err, categoriaDB) => {


            if (err) {
                return response.status(400).json({
                    ok: false,
                    err
                });
            }

            response.json({
                ok: true,
                categoria: categoriaDB
            });

        });
});


// MOSTRAR UNA LAS CATEGORÍA
app.get('/categoria/:id', verificaToken, (request, response) => {

    let id = request.params.id;
    Categoria.findById(id, (err, categoriaDB) => {

        if (err) {
            return response.status(400).json({
                ok: false,
                err: {
                    message: 'No se encontró esa categoría'
                }
            });
        }

        if (categoriaDB === null) {
            response.status(400).json({
                ok: false,
                message: 'Esta categoría no existe'
            });
        } else {
            response.json({
                ok: true,
                categoria: categoriaDB
            });
        }

    })

});


// CREAR UNA LAS CATEGORÍA
app.post('/categoria', [verificaToken, verificaAdmin], (request, response) => {

    let descripcion = request.body.descripcion;
    let usuarioId = request.usuario._id;

    let categoria = new Categoria({
        descripcion,
        usuario: usuarioId
    });

    categoria.save((err, categoriaDB) => {

        if (err) {
            return response.status(400).json({
                ok: false,
                err
            });
        }

        response.json({
            ok: true,
            categoria: categoriaDB
        });
    });


});


// ACTUALIZAR UNA LAS CATEGORÍA
app.put('/categoria/:id', [verificaToken, verificaAdmin], (request, response) => {

    let id = request.params.id;
    let body = _.pick(request.body, 'descripcion');


    Categoria.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, categoriaDB) => {


        if (err) {
            return response.status(400).json({
                ok: false,
                err
            });
        }


        if (categoriaDB === null) {
            response.status(400).json({
                ok: false,
                message: 'Esta categoría no existe'
            });
        } else {
            response.json({
                ok: true,
                categoria: categoriaDB
            });
        }


    });


});


// CREAR UNA LAS CATEGORÍA
app.delete('/categoria/:id', [verificaToken, verificaAdmin], (request, response) => {


    let id = request.params.id;

    Categoria.findByIdAndRemove(id, (err, categoriaDB) => {

        if (err) {
            return response.status(400).json({
                ok: false,
                err
            });
        }

        if (categoriaDB === null) {
            response.status(400).json({
                ok: false,
                message: 'Esta categoría no existe'
            });
        } else {
            response.json({
                ok: true,
                message: 'Categoría borrada'
            });
        }


    });


});




module.exports = app;