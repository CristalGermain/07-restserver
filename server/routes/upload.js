const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();
const Usuario = require('../models/usuario');
const Producto = require('../models/producto');
const fs = require('fs');
const path = require('path');


app.use(fileUpload()); // Para cargar archivos con express


app.put('/upload/:tipo/:id', (request, response) => {

    if (!request.files) {
        return response.status(400).json({
            ok: false,
            err: {
                message: 'No se ha seleccionado ningún archivo'
            }
        });
    }

    // Opciones en el URI permitidas
    let validarTipo = ['productos', 'usuarios'];
    let tipo = request.params.tipo;

    if (validarTipo.indexOf(tipo) < 0) {
        return response.status(400).json({
            ok: false,
            err: {
                message: 'Las opciones son: ' + validarTipo,
                optionReceived: tipo
            }
        });
    }


    // Extensiones permitidas

    let file = request.files.fileInput; //recibe de la misma manera que los parámetros en el 'body'
    let nameFile = file.name.split('.');
    let extensionFile = nameFile[nameFile.length - 1];
    let extensiones = ['png', 'jpg', 'gif', 'jpeg'];

    if (extensiones.indexOf(extensionFile) < 0) {
        // indexOf busca lo que se le indique entre el arreglo
        return response.status(400).json({
            ok: false,
            err: {
                message: ' Las extensiones válidas son: ' + extensiones,
                extensionFile
            }
        });
    }

    let id = request.params.id;
    let newFileName = `${id}-${new Date().getMilliseconds()}.${ extensionFile }`;

    // Guarda un archivo en la dirección especificada
    file.mv(`uploads/${tipo}/${ newFileName }`, (err) => {
        if (err) {
            return response.status(400).json({
                ok: false,
                err
            });
        }

        if (tipo === 'usuarios')
            imagenUsuario(id, response, newFileName);
        else
            imagenProducto(id, response, newFileName);
    });
});


function imagenUsuario(id, response, fileName) {
    Usuario.findById(id, (err, usuarioDB) => {

        if (err) {
            borrarArchivo(fileName, 'usuarios');
            return response.status(500).json({
                ok: false,
                err
            });
        }

        if (!usuarioDB) {
            borrarArchivo(fileName, 'usuarios');
            return response.status(500).json({
                ok: false,
                err: {
                    message: 'El usuario no existe'
                }
            });
        }

        borrarArchivo(usuarioDB.img, 'usuarios');

        usuarioDB.img = fileName;
        usuarioDB.save((err, usuarioDB) => {
            response.json({
                ok: true,
                usuario: usuarioDB,
                img: fileName
            });
        })


    });
}

function imagenProducto(id, response, fileName) {
    Producto.findById(id, (err, productoDB) => {

        if (err) {
            borrarArchivo(fileName, 'productos');
            return response.status(500).json({
                ok: false,
                err
            });
        }

        if (!productoDB) {
            borrarArchivo(fileName, 'productos');
            return response.status(500).json({
                ok: false,
                err: {
                    message: 'El producto no existe'
                }
            });
        }

        borrarArchivo(productoDB.img, 'productos');

        productoDB.img = fileName;
        productoDB.save((err, usuarioDB) => {
            response.json({
                ok: true,
                usuario: usuarioDB,
                img: fileName
            });
        })


    });
}

function borrarArchivo(fileName, tipo) {
    let pathImagen = path.resolve(__dirname, `../../uploads/${tipo}/${fileName}`);

    if (fs.existsSync(pathImagen)) {
        fs.unlinkSync(pathImagen); // para borrar archivo
    }
}

module.exports = app;