const express = require('express');

const { verificaToken, verificaAdmin } = require('../middlewares/autenticacion');

const _ = require('underscore');

const app = express();
const Producto = require('../models/producto');
const Categoria = require('../models/categoria');



// BUSCAR PRODUCTOS
app.get('/productos/buscar/:termino', verificaToken, (request, response) => {

    let termino = request.params.termino;

    // Sirve para regularizar un término
    let regex = new RegExp(termino, 'i'); // Expresión Regular


    // Buscará los registros que contengan el término aun si este contiene sólo un fragmento del nombre registrado
    Producto.find({ nombre: regex, disponible: true })
        .populate('usuario', 'nombre email')
        .populate('categoria', 'descripcion')
        .exec((err, productos) => {
            if (err) {
                return response.status(400).json({
                    ok: false,
                    err
                });
            }

            if (!productos || !productos) {
                return response.status(400).json({
                    ok: false,
                    message: 'Este producto no existe o no está disponible'
                });
            }
            response.json({
                ok: true,
                productos
            });
        });

});




// OBTENER PRODUCTOS
app.get('/productos', verificaToken, (request, response) => {

    let limite = Number(request.query.limite || 5);
    let skip = Number(request.query.desde || 0);

    Producto.find({ disponible: true })
        .populate('usuario', 'nombre email')
        .populate('categoria', 'descripcion')
        .skip(skip)
        .limit(limite)
        .sort('nombre')
        .exec((err, productos) => {
            if (err) {
                return response.status(400).json({
                    ok: false,
                    err
                });
            }

            response.json({
                ok: true,
                productos
            });
        });

});


// OBTENER UN PRODUCTO POR ID
app.get('/productos/:id', verificaToken, (request, response) => {

    let id = request.params.id;
    Producto.findById(id, (err, productoDB) => {
            if (err) {
                return response.status(400).json({
                    ok: false,
                    err
                });
            }

            if (!productoDB || !productoDB.disponible) {
                return response.status(400).json({
                    ok: false,
                    message: 'Este producto no existe o no está disponible'
                });
            }
            response.json({
                ok: true,
                producto: productoDB
            });
        })
        .populate('usuario', 'nombre email')
        .populate('categoria', 'descripcion');

});


// CREAR NUEVO PRODUCTO
app.post('/productos', [verificaToken, verificaAdmin], (request, response) => {

    var categoriaId = null;
    let usuarioId = request.usuario._id;

    const guardarProducto = async() => {

        const categoria = await Categoria.find({ descripcion: request.body.categoria })
            .exec((err, categoriaDB) => {
                if (err) {
                    console.log(categoriaId);
                    return response.status(400).json({
                        ok: false,
                        err
                    });
                } else {
                    categoriaId = categoriaDB[0]._id;
                    console.log(categoriaId);
                }


            });

        const producto = new Producto({
            nombre: request.body.nombre,
            precioUni: request.body.precioUni,
            descripcion: request.body.descripcion,
            disponible: request.body.disponible,
            categoria: categoriaId,
            usuario: usuarioId
        });

        const guardar = producto.save((err, productoDB) => {
            if (err) {
                return response.status(400).json({
                    ok: false,
                    err
                });
            }

            response.json({
                ok: true,
                producto: productoDB
            });
        });
    }


    // Categoria.find({ descripcion: request.body.categoria })
    //     .exec((err, categoriaDB) => {
    //         if (err) {
    //             return response.status(400).json({
    //                 ok: false,
    //                 err
    //             });
    //         } else {
    //             categoriaId = categoriaDB[0]._id;
    //             console.log(categoriaId);

    //             let usuarioId = request.usuario._id;

    //             let producto = new Producto({
    //                 nombre: request.body.nombre,
    //                 precioUni: request.body.precioUni,
    //                 descripcion: request.body.descripcion,
    //                 disponible: request.body.disponible,
    //                 categoria: categoriaId,
    //                 usuario: usuarioId
    //             });

    //             producto.save((err, productoDB) => {
    //                 if (err) {
    //                     return response.status(400).json({
    //                         ok: false,
    //                         err
    //                     });
    //                 }

    //                 response.json({
    //                     ok: true,
    //                     producto: productoDB
    //                 });
    //             });

    //         }
    //     });
    //console.log(categoriaId);



});


// ACTUALIZAR UN PRODUCTO
app.put('/productos/:id', [verificaToken, verificaAdmin], (request, response) => {

    let id = request.params.id;
    let producto = _.pick(request.body, [
        'nombre', 'precioUni', 'descripcion', 'disponible'
    ]);

    //pendiente categoria


    Producto.findByIdAndUpdate(id, producto, { new: true, runValidators: true },
        (err, productoDB) => {
            if (err) {
                return response.status(400).json({
                    ok: false,
                    err
                });
            }
            if (!productoDB || !productoDB.disponible) {
                return response.status(400).json({
                    ok: false,
                    message: 'Este producto no existe o no está disponible'
                });
            }

            response.json({
                ok: true,
                producto: productoDB
            })
        });

});


// BORRAR UN PRODUCTO POR ID (DESHABILITAR)
app.delete('/productos/:id', [verificaToken, verificaAdmin], (request, response) => {

    let id = request.params.id;
    let body = { disponible: false };

    Producto.findByIdAndUpdate(id, body, { new: true, runValidators: true },
        (err, productoDB) => {
            if (err) {
                return response.status(400).json({
                    ok: false,
                    err
                });
            }

            if (!productoDB || !productoDB.disponible) {
                return response.status(400).json({
                    ok: false,
                    message: 'Este producto no existe o ya se encuentra deshabilitado'
                });
            }

            response.json({
                ok: true,
                message: 'Producto deshabilidado'
            })
        });

});


module.exports = app;