const mongoose = require('mongoose');
const uniqueVal = require('mongoose-unique-validator');



let roles = {
    values: ['ADMIN_ROLE', 'USER_ROLE'],
    message: '{VALUE} no es un rol válido'
}



let Schema = mongoose.Schema;

let usuarioSchema = new Schema({
    nombre: {
        type: String,
        required: [true, 'Se requiere el nombre']
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'Se requiere el correo']
    },
    password: {
        type: String,
        required: [true, 'Se requiere una contraseña']
    },
    img: {
        type: String,
        required: false
    },
    role: {
        type: String,
        default: 'USER_ROLE',
        enum: roles
    },
    estado: {
        type: Boolean,
        default: true
    },
    google: {
        type: Boolean,
        default: false
    }
});


// Esto es para hacer modificaciones cuando se imprima el modelo en JSON
usuarioSchema.methods.toJSON = function() {
    let user = this;
    let userObject = user.toObject();
    delete userObject.password;

    return userObject;
};

usuarioSchema.plugin(uniqueVal, { message: '{PATH} debe de ser único' });
module.exports = mongoose.model('Usuario', usuarioSchema);