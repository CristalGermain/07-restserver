
## REST Server con Node

Para utilizar la aplicación, es necesario instalar el manejador de paquetes con el siguiente comando:

```
npm install

```

SECCIÓN 8

En esta sección, se configuró un server REST por medio de el paquete express (que usa HTTP). Este server puede implementarse para el uso de APIs, ya que se estableció respuesta por el método POST, GET, PUT y DELETE. En cada método se utilizan de manera similar el request y la respuesta. Hay una pequeña diferencia en POST/PUT: para poder obtener los datos que nos mandan mediante la URL, es importante convertirlos a datos manejables. En este caso, se utiliza el paquete Body-parser para obtener un JSON con estos parámetros.
El server se encuentra en un repositorio de Heroku.

SECCIÓN 9

- mongoose
- mongoose-unique-validator
- bcrypt
- underscore

* mongo atlas (MongoDB Compass)

En esta sección se utilizó moongose, que es una librería para conectar se una base de datos con MongoDB, así como mongoose-unique-validator para hacer diferentes validaciones. Se realizó una base de datos llamada 'cafe' donde se trabajó con una colección llamada 'usuarios'. Para trabajar con ello, se realizaron diferentes peticiones (GET, POST, PUT y DELETE) bajo un modelo que contenía la estructura del usuario.

En el modelo de Usuario, en el esquema, se añadieron varias propiedades para validar los datos, como la propiedad 'unique' para validar que cierto valor no se repita; la propiedad 'enum', para validar el recibir sólo los valores deseados; la propiedad 'required' para validar que un dato es obligatorio. Todo esto arroja mensajes de errores o bien el resultado, lo cual es necesario para interactuar con el usuario que utilice nuestro sistema.

Para cada tipo de petición se realizó un proceso similar. En el caso del método POST, que se utilizó para agregar datos a la BD de Mongo; se utilizó el paquete bcrypt para encriptar la contraseña. En todas las peticiones se utilizaron funciones de moongose para llevarlas a cabo.

Por último, cuando el CRUD estuvo listo, se conectó la base de datos local con un clúster en la nube para almacenarla, gracias a MongoDB Compass. Esto complementando al repositorio que se tenía en Heroku.

Y, para proteger los datos privados como credenciales y URI de conexión al subir código con Git, se utilizaron las variables de entorno de Heroku, ya que es posible personalizarlas. Ejemplos de ello son los siguientes:

```
heroku config:set nombre="Cristal"
heroku config:get nombre
heroku config:unset nombre
```

De esta manera, estos datos se pueden establecer desde el dispositivo del desarrollador y se registran en Heroku, por lo que no es necesario estar conectandose todo el tiempo.

SECCIÓN 10

En esta sección se abordó el tema de tókens. Se generó un token para manejar sesiones dentro del sistema. Para esto, se utilizó el paquete jsonwebtoken, con lo cual se realizó un login. Si los datos brindados son correctos, se genera un token, cuyo payload contiene esos datos, si no, no se genera el token. De no tener un token, no es posible consumir otros servicios, ya que se realizó un método de autenticación de token, el cual se usó como middleware. 

SECCIÓN 11

Una vez teniendo un login para usuarios registrados manualmente, se utilizó una API de Google para autenticar por medio del mismo. Esto gracias al paquete google-auth-library, que permite una autenticación mediante la cuenta de Google del usuario. Para hacer esto, se utilizó la consola de APIs de Google y las funciones que ofrece Google para manejar su API. Una vez hecho esto, se creó un servicio POST donde se verifican los datos del request para crear un nuevo token al iniciar sesión mediante Google en caso de no encontrar coincidencias en la DB de Mongo.

Como Tip, después de generar los servicios GET, POST, PUT y DELETE de los usuarios, se utilizó una herramienta de Postman para publicar la documentación de los servicios del server, ya que se crea una documentación automática muy útil.

SECCIÓN 12

En esta sección se continuó con el server agregando servicios POST, GET, PUT y DELETE para un modelo Categoría y otro Producto. La novedad en esta sección, aparte de la práctica, es la función "populate", que ofrece información de un objeto con base en un Id, además de la función "sort", para ordenar. Además, otra novedad es la regularización de las expresiones para comparar esas expresiones con el parámetro registrado en la DB que se señale. Esto con la instancia de RegExp().
